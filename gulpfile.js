const { series, parallel, src, dest, watch, task } = require('gulp');
const rename = require('gulp-rename');
var sass = require('gulp-dart-sass');
const bs = require('browser-sync').create();
const babel = require('gulp-babel');
const uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');

//sass.compiler = require('sass');

function sassTask() {
  return src('src/scss/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({ outputStyle: 'compressed' }))
    .pipe(sourcemaps.write('.'))
    .pipe(bs.stream({match: '**/*.css'}))
    .pipe(dest('./dist/css/'))
}

function jsTask() {
  return src('src/js/*.js')
    .pipe(sourcemaps.init())
    .pipe(babel({
      presets: ['@babel/env']
    }))
    .pipe(uglify({}))
    .pipe(rename({ suffix: '.min' }))
    .pipe(sourcemaps.write('.'))
    .pipe(dest('./dist/js/'))
    .pipe(bs.stream({match: 'src/js/*.js'}))
}

function bsTask() {
  bs.init({
    server: {
      baseDir: "./"
    }
  });

  watch('./src/scss/*.scss', sassTask).on('change', bs.reload);
  watch("./src/js/*.js", jsTask).on('change', bs.reload);
  watch("./*.html").on('change', bs.reload);
};

exports.default = series(parallel(sassTask, jsTask), bsTask)
