/** Class representing a carousel. */
class Carousel {
/**
 * 
 * @param {Object} options
 * @param {String} options.container Caarousel root container Id
 * @param {String} options.icon Carousel header icon (Material icon string)
 * @param {String} options.title Carousel header title
 * @param {String} options.subtitle Carousel header subtitle
 * @param {Function} options.fetchCards Function to retrive cards data from API
 */ 
  constructor(options) {
      Object.assign(this, options); //assign option to new carousel
      this.init();
  }

  /** Initialize the new carousel */
  init = () => {
    this.chunkSize = 8;  // Desired chunksize (Number of required cards)
    this.progress = 0;   // Initial progress value (translation of carousel track if track is bigger than carousel viewport)
    this.dragging = false; // Initial carousel dragging state, set to false for disallow interaction on carousel
    this.carouselContainer = document.getElementById(this.container);                                               // get the carousel parent container
    this.carouselContainer.insertAdjacentHTML('afterbegin', this.carouselTpl(this.icon,this.title,this.subtitle));  // append carousel template to parent and set icon,title,subtitle from options

    // get all required node from carousel, necessary for the next operations
    this.carousel = this.carouselContainer.querySelector('.carousel-container');
    this.carouselInner = this.carousel.querySelector('.carousel-inner');
    this.track = this.carousel.querySelector('.track');
    this.nav = this.carousel.querySelector('.nav');
    this.prev = this.carousel.querySelector('.prev');
    this.next = this.carousel.querySelector('.next');

    // append cards plaaceholder to the track node based on required chunkSize
    for ( let i = 0; i < this.chunkSize; i++) {
      this.track.insertAdjacentHTML('afterbegin', this.cardTpl());
    }

    // calculate the single card width
    this.cardSize = this.track.clientWidth / this.chunkSize;

    // retrive random data from cardsData object based on fetchCards function and update cards content
    this.fetchCards(this.chunkSize)
    .then(data => this.updateCards(data))
    .catch(err => { console.log(err) });

  }

  /** apply all necesary event to carousel node for interaction */
  event = () => {
    this.carousel.addEventListener('mouseenter', this.carouselShowCtl);
    this.carousel.addEventListener('mouseleave', this.carouselHideCtl);
    this.carouselInner.addEventListener('wheel', this.carouselMoveWheel);

    this.carouselInner.addEventListener('mousedown', this.carouselMoveStart);
    this.carouselInner.addEventListener('mousemove', this.carouselMove);
    this.carouselInner.addEventListener('mouseup', this.carouselMoveEnd);
    this.carouselInner.addEventListener('mouseleave', this.carouselMoveEnd);

    this.carouselInner.addEventListener('touchstart', this.carouselMoveStart);
    this.carouselInner.addEventListener('touchmove', this.carouselMove);
    this.carouselInner.addEventListener('touchend', this.carouselMoveEnd);

    this.next.addEventListener('click',this.carouselBtnClick);
    this.prev.addEventListener('click',this.carouselBtnClick);
  }

  /**
   * Show carousel control and interaction
   */
  carouselShowCtl = () => {
    if (this.maxScroll > 0 ) {
      this.nav.classList.add('active');
      this.carouselInner.classList.add('grabbable');
    }
  }
  /**
   * Hide carousel control and interaction
   */
  carouselHideCtl = () => {
    if (this.nav.classList.contains('active') && this.carouselInner.classList.contains('grabbable')) {
      this.nav.classList.remove('active');
      this.carouselInner.classList.remove('grabbable');
    }  
  }
   /**
   * Manage click event on prev/next button
   * @param {Object} e Event
   */
  carouselBtnClick = (e) =>{
    e.preventDefault();
    let direction = e.currentTarget.classList.contains('next') ? 1 : -1;
    this.progress += this.cardSize * direction;
    this.move();
  }

   /**
   * Manage movestart event, set the cursor initial postion and set dragging carousel state to true
   * @param {Object} e Event
   */
  carouselMoveStart = (e) => {
    e.preventDefault();
    this.startX = e.clientX || e.touches[0].clientX;
    this.dragging = true
  }

   /**
   * Manage mousemove event
   * @param {Object} e Event
   */
  carouselMove = (e) => {
    e.preventDefault();
    if (!this.dragging) return false;
    let x = e.clientX || e.touches[0].clientX;
    this.progress += (this.startX - x) * 2.5;    // calculate the distance between the movement start and movement stop, multiply for more speed
    this.startX = x;                             // update start position
    this.move();
  }

  /** Set carousel drag state to false */
  carouselMoveEnd = (e) => {
    e.preventDefault();
    this.dragging = false;
  }
  
   /**
   * Manage carousel scroll on mouse wheel
   * @param {Object} e Event
   */
  carouselMoveWheel = (e) => {
    e.preventDefault();
    this.progress += e.deltaY;
    this.move();
  }

  /** Do the animation, translate track node based on new position */
  requestAnimation = () => {
    requestAnimationFrame(this.requestAnimation);
    this.track.style.transform = `translateX(${-this.progress}px)`;
  }

  /** Set the new track node position between 0 to maxscroll and manage show/hide navigation buttons */
  move = () => {
    this.progress = Math.max(0, Math.min(this.progress, this.maxScroll));
    this.progress > 0 ? this.prev.classList.remove('hide') : this.prev.classList.add('hide');
    this.progress == this.maxScroll ? this.next.classList.add('hide') : this.next.classList.remove('hide');
  }

    /**
   * Convert duration to human readable format
   * @param {Number} Seconds to convert
   * @returns {String} Human readable string
   */
  formatDuration = (s) => {
    if (s < 0) s = -s;
    const time = {
      d: Math.floor(s / 86400),
      h: Math.floor(s / 3600) % 24,
      m: Math.floor(s / 60) % 60
    };

    return Object.entries(time)
      .filter(val => val[1] !== 0)                                      // Remove 0 values entry
      .map(val => val[1] + '' + (val[1] !== 1 ? val[0] + 's' : val[0])) // Append "s" if value bigger than 1
      .join(' ');
  }

/**
 * Update cards content,set fadeout animation for non used placeorder cards and calculate the maximum allowed scroll
 * @param {Array} cardsData Object contain cards information
 * @param {String} cardsData[].image Card image path/url
 * @param {String} cardsData[].type Card content type
 * @param {Number} cardsData[].duration Duration in seconds
 * @param {String} cardsData[].title Card title
 * @param {String} cardsData[].cardinality Card tipe, single or collection
 */
  updateCards = (cardsData) => {
    let cards = this.carousel.querySelectorAll('.card');
    this.maxScroll = (this.cardSize * cardsData.length) - this.carouselInner.clientWidth;

          // remove unused cards node after fadeout animation end
          this.track.onanimationend = (el) => {
            if(el.animationName === 'cardFadeOut') el.target.remove();
          }
          
          // if track node width is bigger than carousel container add interaction event
          if (this.maxScroll > 0 ) {
              this.event();
              this.requestAnimation();
          }

          // Update content foreach placeholter cards, if no index found hide unused cards
          cards.forEach((card,index) => {
            if (cardsData[index]){
              card.querySelector('.image').src = cardsData[index].image;
              card.querySelector('.type').textContent = cardsData[index].type;
              card.querySelector('.duration').textContent =  this.formatDuration(cardsData[index].duration);
              card.querySelector('.card-title').textContent = cardsData[index].title;
              if (cardsData[index].cardinality === "collection") card.classList.add('collection');
              card.classList.remove('loading');
            }else{
              card.classList.add('hide');
            }
          })

          // if no card to display append a message error on the carousel container
          if (cardsData.length === 0){
            this.track.remove();
            this.carouselInner.insertAdjacentHTML('afterbegin', this.errorTpl('No items to display'));
          }
  }
/**
 * Carousel html template
 * @param {String} icon  Material icon string
 * @param {String} title Carousel Title
 * @param {String} subtitle Carousel Subtitle
 * @returns {String} Html template of new carousel
 */ 
  carouselTpl = (icon,title,subtitle) => {
    return `
      <header class='carousel-header'>
        <i class="icon material-icons">
          ${icon}
        </i>
        <h1 class='title'>${title}</h1>
        <h3 class='subtitle'>${subtitle}</h3>
      </header>
      <main class="carousel-container">
        <div class="carousel-inner">
          <div class="track">
          </div>          
        </div>
        <nav class="nav">
            <button class="prev hide">
              <i class="material-icons">
                keyboard_arrow_left
              </i>
            </button>
            <button class="next">
              <i class="material-icons">
                keyboard_arrow_right
              </i>
            </button>
          </nav>
      </main>`;
  }
/**
 * Error html template
 * @param {String} message Custom message string to show
 * @returns {String} Error html template
 */ 
  errorTpl = (message) => {
    return `<div class="error"><p> ${message}</p></div>`;
  }
/**
 * Single card html template
 * @returns {String} card placeholder html template
 */ 
  cardTpl = () => {
    return `
    <div class="card loading">
      <figure class="card-image">
        <img class="image">
        <p class="type"></p>
        <p class="duration"></p>
      </figure>
      <div class="card-detail">
        <h2 class="card-title"></h2>
        <p class="card-description"></p>
      </div>
    </div>`;
  }
}